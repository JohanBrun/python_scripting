#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import argparse
from urllib.parse import urlparse, urljoin


def brklnk(link, depth):
    if depth >= 0:
        request = requests.get(link)
        if request.status_code >= 400:
            print(link, '\t', request.status_code, '\t broken link !')
        else:
            html_page_content = request.text
            soup = BeautifulSoup(html_page_content, "html.parser")

            for links in soup.find_all('a'):
                href = links.get('href')
                href_split = urlparse(href)

                if href_split.scheme == '':
                    link = urljoin(link, href)
                    
                brklnk(link, depth-1)


if __name__=="__main__":

    parser = argparse.ArgumentParser(description="Lis le contenu d'une page HTML et trouve les liens cassés")
    parser.add_argument("url", help="préciser l'url")
    parser.add_argument("-d", "--depth", help="n niveau de profondeur de recherche (défault 1)", default=1, type=int)
    args = parser.parse_args()

    brklnk(args.url, args.depth)