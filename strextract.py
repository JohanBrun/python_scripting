#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import re
import argparse

def main():
	'''Arguments'''
    parser = argparse.ArgumentParser(description="Extrait toutes les strings des fichiers du dossier source en argument")
    parser.add_argument("rootdir", help="Chemin relatif ou absolu")
    parser.add_argument("-s", "--suffix", help="limite la recherche aux fichiers avec ce suffixe")
    parser.add_argument("-p", "--path", action="store_true", help="chaque ligne est précédée du chemin du fichier associé")
    parser.add_argument("-a", "--all", action="store_true", help="prendre en compte les fichiers cachés")
    args = parser.parse_args()
    str_extract(args)

def str_extract(args):
    results = re.compile(r"\"(.*)\"|\'(.*)\'")

    for path, _, filenames in os.walk(args.rootdir):
        for filename in filenames:
            if not args.all and filename.startswith('.'):
                continue
            if args.suffix:
                if not filename.endswith(args.suffix):
                    continue
                """ if not args.all:
                    if filename.startswith('.'):
                        continue """

            pathname = os.path.join(path, filename)
            try:
                with open(pathname) as file:
                    for line in file:
                        for match in results.finditer(line):
                            if args.path:
                                print(pathname, "\t", match.group(0))
                            else:
                                print(match.group(0))
            except:
                pass

if __name__ == '__main__':
    main()
